
// Action is something you need to declare what type of case in reduser needs to execute ,
//  so we have to dispatch the action to perform redux state updation from reduser

// type : name of action for identify by reduser
// payload : (optional) value for reduser to update the redux state accordingly 

export const increment = (count) =>{
    return {
        type:"Increment",
        payload:count
    }
}
export const decrement = (count) =>{
    return {
        type:"Decrement",
        payload:count
    }
}
