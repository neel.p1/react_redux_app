const initialState ={
    count:0,
}
const CountReduser = (state=initialState,action) =>{
    switch(action.type){
        case "Increment": 
            return{
                ...state,
                count : action.payload + 1,
            }
        case "Decrement": 
            return{
                ...state,
                count : action.payload - 1,
            }
        default : 
            return state
    }
}

export default CountReduser;
