import { combineReducers } from "redux";
import CountReduser from "./countReduser";

// redux store accept only one reduser ,
// so we need to combine all the reduser in one rootReduser
// here {AllCount:CountReduser} in this AllCount is name of reduser which will use to access  reduser ehich is "optional"

const rootReduser = combineReducers({AllCount:CountReduser});

export default rootReduser;