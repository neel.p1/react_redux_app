import React, { useState, useEffect } from 'react';
import { useDispatch , useSelector} from 'react-redux';
import { increment, decrement } from "./actions/";

function ReduxComponent() {
    const [count,setCount] = useState(0);
    const dispatch = useDispatch();
    const selector = useSelector(state => state.AllCount.count);
    useEffect(() => {
        console.log(selector);
        setCount(selector);
    }, [selector]);


    // const strArray = ["sanjay","code","react","amaizing","whatever"];
    // const numArray = [34, 11, 5, 37, 86, 54];
    
    //   function compareLogic(a, b) {
    //     if (a > b) {
    //       return 1;
    //     }
    //     if (a < b) {
    //       return -1;
    //     }
    //     return 0;
    //   }
    
    //   const sorting =()=> {
    //     let stringUpdateArray = strArray.sort(compareLogic);
    //     alert(stringUpdateArray);
        
    //     let numberUpdateArray = numArray.sort(compareLogic);
    //     alert(numberUpdateArray);
    // }

    // const duplicateArray = [2, 5, 32, 1, 2, 856, 32, 5, 99];

    // const duplicateRemove = () =>{
    //     let newArray = new Set(duplicateArray)
    //     alert([...newArray]);
    // }


    return (
        <div>
            <h1>{selector}</h1>
            <button onClick={()=>{
                dispatch(increment(count));
            }}>Increment</button>
            <button onClick={()=>{
                dispatch(decrement(count));
            }}>Decrement</button>

            <br/>
            {/* <h1>StrArray</h1> */}
      {/* <button onClick={sorting}>Sort</button>
      <button onClick={duplicateRemove}>Duplicate Remove</button> */}
        </div>
    )
}

export default ReduxComponent
